#include "stdafx.h"
#include "GameStart.h"
#include "Renderer.h"
#include "ObjectA.h"
#include "Dependencies\glew.h"


Start::Start()
{
	m_Renderer = new Renderer(1000, 500);

	BackGround = m_Renderer->CreatePngTexture("./startbackground.png");
}


Start::~Start()
{
}

void Start::RenderScene(float etime)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);

	//float x = std::sin(time)*200.f;
	//float y = std::cos(time)*200.f;
	//time += 0.01f;

	m_Renderer->DrawTextureRect(0, 0, 0, 1000, 500, 1, 1, 1, 1, BackGround);

}