/*
Copyright 2017 Lee Taek Hee (Korea Polytech University)

This program is free software: you can redistribute it and/or modify
it under the terms of the What The Hell License. Do it plz.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY.
*/

#include "stdafx.h"
#include <iostream>
#include <windows.h>
#include "Dependencies\glew.h"
#include "Dependencies\freeglut.h" // 씬매니저에 넣지 않기

#include "ScnMgr.h"
#include "GameStart.h"
#include "GameOver.h"
#include "Sound.h"

// object : 체력, 위치, 크기

ScnMgr *g_ScnMgr = NULL;
Start *g_Start = NULL;
LOSE *g_Over = NULL;
DWORD g_PrevRenderTime = 0;

bool GameStart = false;
bool GameOver = false;

bool g_KeyW = false; // up : false, down : true
bool g_KeyS = false;
bool g_KeyA = false;
bool g_KeyD = false;
bool g_KeySP = false;
float firstTime = -1.f;

int g_Shoot = SHOOT_NONE;

void PlayGame();
void OverGame(int state);
void StartGame();
void RenderScene(void)
{
	if (g_PrevRenderTime == 0) // Initialize
	{
		g_PrevRenderTime = timeGetTime(); //ms
		firstTime = (float)timeGetTime() / 1000.f;
	}

	//Elapsed Time
	DWORD currentTime = timeGetTime();
	DWORD elapsedTime = currentTime - g_PrevRenderTime;
	g_PrevRenderTime = currentTime;
	float eTime = (float)elapsedTime / 1000.f; // convert to second

	//std::cout << "elapsed time : " << ((float)timeGetTime() / 1000.f) - firstTime << "\n";

	float forceX = 0.f, forceY = 0.f, forceZ = 0.f;
	float amount = 0.2f;
	float amountZ = 13.f;

	if (g_KeyW)
	{
		forceY += amount;
	}
	if (g_KeyS)
	{
		forceY -= amount;
	}
	if (g_KeyA)
	{
		forceX -= amount;
	}
	if (g_KeyD)
	{
		forceX += amount;
	}
	if (g_KeySP)
	{
		forceZ += amountZ;
	}

	g_ScnMgr->ApplyForce(forceX, forceY, forceZ, eTime, 0); //elapsed time 인자 추가
	g_ScnMgr->Update(eTime);
	g_ScnMgr->RenderScene(eTime);
	g_ScnMgr->Shoot(g_Shoot);
	g_ScnMgr->UpdateCollision();
	g_ScnMgr->DoGarbageCollet();
	g_ScnMgr->AddEnemybyTime(((float)timeGetTime() / 1000.f) - firstTime);

	if (g_ScnMgr->GetGameIsEnd() == true)
	{
		std::cout << "gameover";
		GameOver = true;
		GameStart = false;
		delete g_ScnMgr;
		OverGame(0);
	}
	if (g_ScnMgr->GetGameIsWin() == true)
	{
		std::cout << "gamewin";
		GameOver = true;
		GameStart = false;
		delete g_ScnMgr;
		OverGame(1);
	}

	glutSwapBuffers();
}

void RenderStart()
{
	if (g_PrevRenderTime == 0) // Initialize
	{
		g_PrevRenderTime = timeGetTime(); //ms
		firstTime = (float)timeGetTime() / 1000.f;
	}

	//Elapsed Time
	DWORD currentTime = timeGetTime();
	DWORD elapsedTime = currentTime - g_PrevRenderTime;
	g_PrevRenderTime = currentTime;
	float eTime = (float)elapsedTime / 1000.f; // convert to second


	g_Start->RenderScene(eTime);

	glutSwapBuffers();
}
void RenderOver()
{
	if (g_PrevRenderTime == 0) // Initialize
	{
		g_PrevRenderTime = timeGetTime(); //ms
		firstTime = (float)timeGetTime() / 1000.f;
	}

	//Elapsed Time
	DWORD currentTime = timeGetTime();
	DWORD elapsedTime = currentTime - g_PrevRenderTime;
	g_PrevRenderTime = currentTime;
	float eTime = (float)elapsedTime / 1000.f; // convert to second


	g_Over->RenderScene(eTime);

	glutSwapBuffers();
}
void Idle(void)
{
	if (GameStart == true && GameOver == false)
	{
		RenderScene();
	}
	else if(GameStart == false && GameOver == false)
	{
		RenderStart();
	}
	else if(GameStart == false && GameOver == true)
	{
		RenderOver();
	}
}

void MouseInput(int button, int state, int x, int y)
{
	if (GameStart == true)
	{
		RenderScene();
	}
}

// click : 해당되는 정확한 위치에서 벗어나지 않은 상태에서 up 혹은 down이 되는 것
// hold down

void KeyDownInput(unsigned char key, int x, int y)
{
	if (GameStart == true && GameOver == false)
	{
		if (key == 'w')
		{
			g_KeyW = true;
		}

		if (key == 's')
		{
			g_KeyS = true;
		}

		if (key == 'a')
		{
			g_KeyA = true;
		}

		if (key == 'd')
		{
			g_KeyD = true;
		}

		if (key == ' ')
		{
			g_KeySP = true;
		}
	}
	else if(GameStart == false && GameOver == false)
	{
		if (key == ' ')
		{
			g_KeyW = false; // up : false, down : true
			g_KeyS = false;
			g_KeyA = false;
			g_KeyD = false;
			g_KeySP = false;
			GameStart = true;
			GameOver = false;
			delete g_Start;
			PlayGame();
		}
	}
	else if (GameStart == false && GameOver == true)
	{
		if (key == ' ')
		{
			GameStart = false;
			GameOver = false;
			delete g_Over;
			StartGame();
		}
	}
}

void KeyUpInput(unsigned char key, int x, int y)
{
	if (GameStart == true && GameOver == false)
	{
		if (key == 'w')
		{
			g_KeyW = false;
		}

		if (key == 's')
		{
			g_KeyS = false;
		}

		if (key == 'a')
		{
			g_KeyA = false;
		}

		if (key == 'd')
		{
			g_KeyD = false;
		}

		if (key == ' ')
		{
			g_KeySP = false;
		}
	}
}

void SpecialKeyUpInput(int key, int x, int y)
{
	if (GameStart == true && GameOver == false)
	{
		g_Shoot = SHOOT_NONE;
	}
}
void SpecialKeyDownInput(int key, int x, int y)
{
	if (GameStart == true && GameOver == false)
	{
		if (key == GLUT_KEY_LEFT)
		{
			g_Shoot = SHOOT_LEFT;
		}
		if (key == GLUT_KEY_UP)
		{
			g_Shoot = SHOOT_UP;
		}
		if (key == GLUT_KEY_RIGHT)
		{
			g_Shoot = SHOOT_RIGHT;
		}
		if (key == GLUT_KEY_DOWN)
		{
			g_Shoot = SHOOT_DOWN;
		}

		RenderScene();
		g_Shoot = SHOOT_NONE;
		RenderScene();
	}
}

void StartGame()
{
	glutSetKeyRepeat(GLUT_KEY_REPEAT_OFF); // repeat off

	glutDisplayFunc(RenderStart);
	glutIdleFunc(Idle);
	glutKeyboardFunc(KeyDownInput);
	glutKeyboardUpFunc(KeyUpInput);

	glutSetKeyRepeat(GLUT_KEY_REPEAT_OFF);

	g_Start = new Start();

	glutMainLoop();
}
void OverGame(int state)
{
	glutSetKeyRepeat(GLUT_KEY_REPEAT_OFF); // repeat off

	glutDisplayFunc(RenderOver);
	glutIdleFunc(Idle);
	glutKeyboardFunc(KeyDownInput);
	glutKeyboardUpFunc(KeyUpInput);

	glutSetKeyRepeat(GLUT_KEY_REPEAT_OFF);

	g_Over = new LOSE(state);

	glutMainLoop();
}
void PlayGame()
{
	glutSetKeyRepeat(GLUT_KEY_REPEAT_OFF); // repeat off

	glutDisplayFunc(RenderScene);
	glutIdleFunc(Idle);
	glutKeyboardFunc(KeyDownInput);
	glutKeyboardUpFunc(KeyUpInput);
	glutMouseFunc(MouseInput);
	//glutSpecialFunc(SpecialKeyInput);
	glutSpecialFunc(SpecialKeyDownInput);
	glutSpecialUpFunc(SpecialKeyUpInput);

	glutSetKeyRepeat(GLUT_KEY_REPEAT_OFF);

	g_ScnMgr = new ScnMgr();

	return;

	glutMainLoop();
}

int main(int argc, char **argv)
{
	// Initialize GL things
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowPosition(0, 0);
	glutInitWindowSize(1000, 500);
	glutCreateWindow("Game Software Engineering KPU");

	glewInit();
	if (glewIsSupported("GL_VERSION_3_0"))
	{
		std::cout << " GLEW Version is 3.0\n ";
	}
	else
	{
		std::cout << "GLEW 3.0 not supported\n ";
	}

	StartGame();

	delete g_ScnMgr;
	return 0;
}