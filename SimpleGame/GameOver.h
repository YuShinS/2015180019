#pragma once
#include "Renderer.h"

class LOSE
{
private:
	Renderer *m_Renderer; // Renderer는 하나만 있으면 됨

	GLuint LoseBackGround = 0;
	GLuint WinBackGround = 0;

	int gamestate = 0; // 0이면 게임오버, 1이면 게임클리어

public:
	LOSE(int state);
	~LOSE();

	void RenderScene(float etime);
};