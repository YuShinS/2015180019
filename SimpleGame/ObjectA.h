#pragma once

class ObjectA
{
private:
	//Variable
	float pos_X;
	float pos_Y;
	float pos_Z;

	float sizeX;
	float sizeY;
	float sizeZ;

	float m_VelX;
	float m_VelY;
	float m_VelZ;

	float m_AccX;
	float m_AccY;
	float m_AccZ;
	//float m_ForceX;
	//float m_ForceY;
	float m_Mass;
	float m_coefFriction;
	float m_R, m_G, m_B, m_A;
	float m_HP;

	int m_Kind;
	int m_State;

public:
	ObjectA();
	~ObjectA();

	//function
	void GetPosition(float *x, float *y, float *z);
	void SetPosition(float x, float y, float z);
	void GetSize(float *sx, float *sy, float *sz);
	void SetSize(float sx, float sy, float sz);
	void GetVelocity(float *vx, float *vy, float *vz);
	void SetVelocity(float vx, float vy, float vz);
	void GetAcc(float *vx, float *vy, float *vz);
	void SetAcc(float vx, float vy, float vz);
	//void GetForce(float *x, float *y);
	//void SetForce(float x, float y);
	void GetMass(float *x);
	void SetMass(float x);
	void GetFrictionCoef(float *x);
	void SetFrictionCoef(float x);
	void Update(float eTimeInSec);
	void ApplyForce(float x, float y, float z, float time);
	void GetColor(float *r, float *g, float *b, float *a);
	void SetColor(float r, float g, float b, float a);
	void GetKind(int *kind);
	void SetKind(int kind);
	void GetHP(float *hp);
	void SetHP(float hp);
	void GetState(int *st);
	void SetState(int st);
};