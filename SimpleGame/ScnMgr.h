#pragma once
#include "Renderer.h"
#include "global.h"
#include "ObjectA.h"
#include "Sound.h"

class ScnMgr
{
private:
	Renderer *m_Renderer; // Renderer는 하나만 있으면 됨
						  // ObjectA *m_TestObj;

	Sound *m_Sound;
	GLuint BackGround = 0;
	GLuint m_TestTexture = 0;
	GLuint m_TestExplodeTexture = 0;
	GLuint m_TestIsaacTexture = 0;
	GLuint m_TestLittleDevilTexture = 0;
	GLuint m_TestTurretTexture = 0;
	GLuint m_TestFlyTexture = 0;
	GLuint m_TestBossTexture = 0;
	GLuint m_TestBossPunchTexture = 0;
	ObjectA *m_Objects[MAX_OBJECTS];

	int m_SoundBG = 0;
	int m_SoundFire = 0;
	int m_SoundExplosion = 0;
	int m_SoundHit = 0;
	int m_SoundWarn = 0;
	int m_SoundBossRoar = 0;

	bool Boss = false;
	bool gameend = false;
	bool gamewin = false;

	float LittledevilTime = 0.f;
	float FlyTime = 0.f;
	float BossTime = 0.f;
	float PunchTime = 0.f;

public:
	ScnMgr();
	~ScnMgr();

	bool GetGameIsEnd();
	bool GetGameIsWin();
	void RenderScene(float etime);
	void Update(float eTimeInSec);
	void ApplyForce(float x, float y, float z, float time, int Objectnum);
	void AddObject(float px, float py, float pz, float sx, float sy, float sz, float vx, float vy, float vz, int kind, int HP, int state);

	void AddEnemybyTime(float time);
	void AddLittleDevil();
	void LittleDevilMove(int ObjectNum, float etime);
	void AddTurret();
	void TurretbulletShot(int ObjectNum, float etime);
	void AddBoss();
	void BossMove(int ObjectNum, float etime);
	void AddPunch();
	void PunchMove(int ObjectNum, float etime);

	void DeleteObject(unsigned int id);
	int FindEmptyObjectSlot();
	void Shoot(int shootID);
	bool BBCollision(float minx, float miny,
		float maxx, float maxy,
		float minz, float maxz,
		float minx1, float miny1,
		float maxx1, float maxy1,
		float minz1, float maxz1);
	void ProcessCollision(int i, int j);
	void UpdateCollision();
	void DoGarbageCollet();
};