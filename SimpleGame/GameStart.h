#pragma once
#include "Renderer.h"

class Start
{
private:
	Renderer *m_Renderer; // Renderer는 하나만 있으면 됨
						  // ObjectA *m_TestObj;

	GLuint BackGround = 0;

public:
	Start();
	~Start();

	void RenderScene(float etime);
};