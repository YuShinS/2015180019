#include "stdafx.h"
#include "ScnMgr.h"
#include "Renderer.h"
#include "ObjectA.h"
#include "Dependencies\glew.h"

//1.Object 동적할당
// m_TestObj -> m_Objs[MAX_OBJECTS] (리스트)
// AddObject()
// DeleteObject()
//2.Render Scene
// 모든 objects 중 bRender == true 무조건 그리게 함
// for문을 돌면서 모든 object 렌더링

// 충돌 penetration (관통) - 충돌체크가 point-plane이 아닌 line-plane
// 충돌처리
// 1. rect-rect collision detection
// 2. 

ScnMgr::ScnMgr()
{
	//if (!m_Renderer->IsInitialized())
	//{
	//	std::cout << "Renderer could not be initialized.. \n";
	//}

	// Init Object
	for (int i = 0; i < MAX_OBJECTS; ++i)
	{
		m_Objects[i] = NULL;
	}

	// Create Hero Object
	m_Objects[HERO_ID] = new ObjectA();
	m_Objects[HERO_ID]->SetPosition(0.f, 0.f, 0.0f);
	m_Objects[HERO_ID]->SetVelocity(0.f, 0.f, 0.f);
	m_Objects[HERO_ID]->SetAcc(0.f, 0.0f, 0.f);
	m_Objects[HERO_ID]->SetSize(0.4f, 0.4f, 0.4f);
	//m_Objects[HERO_ID]->SetForce(0.f, 0.f);
	m_Objects[HERO_ID]->SetMass(0.05f);
	m_Objects[HERO_ID]->SetFrictionCoef(0.1f); // 0.5f
	m_Objects[HERO_ID]->SetColor(1.f, 1.f, 1.f, 1.f);
	m_Objects[HERO_ID]->SetKind(KIND_HERO);
	m_Objects[HERO_ID]->SetState(STATE_GROUND);
	m_Objects[HERO_ID]->SetHP(3000.f);

	AddLittleDevil();
	AddTurret();

	m_Renderer = new Renderer(1000, 500);

	BackGround = m_Renderer->CreatePngTexture("./background.png");

	// Create Textures
	m_TestTexture = m_Renderer->CreatePngTexture("./shadow.png"); // 따로 구해서 png파일 넣기
	m_TestExplodeTexture = m_Renderer->CreatePngTexture("./explode.png");
	m_TestIsaacTexture = m_Renderer->CreatePngTexture("./isaac.png");
	m_TestLittleDevilTexture = m_Renderer->CreatePngTexture("./Boss_Baby.png");
	m_TestTurretTexture = m_Renderer->CreatePngTexture("./turret.png");
	m_TestFlyTexture = m_Renderer->CreatePngTexture("./Fly.png");
	m_TestBossTexture = m_Renderer->CreatePngTexture("./Boss.png");
	m_TestBossPunchTexture = m_Renderer->CreatePngTexture("./punch.png");

	m_Sound = new Sound();
	m_SoundBG = m_Sound->CreateSound("./Sounds/bgm.mp3");
	m_SoundFire = m_Sound->CreateSound("./Sounds/shoot.mp3");
	m_SoundExplosion = m_Sound->CreateSound("./Sounds/explosion.mp3");
	m_SoundHit = m_Sound->CreateSound("./Sounds/gulp.mp3");
	m_SoundWarn = m_Sound->CreateSound("./Sounds/warning.WAV");
	m_SoundBossRoar = m_Sound->CreateSound("./Sounds/BossRoar.mp3");

	m_Sound->PlaySound(m_SoundBG, true, 3.f);
}



ScnMgr::~ScnMgr()
{
	//delete m_Renderer;
	//delete m_Sound;
	//delete m_Objects;
}

int g_Seq = 0;

bool ScnMgr::GetGameIsEnd()
{
	return gameend;
}
bool ScnMgr::GetGameIsWin()
{
	return gamewin;
}

void ScnMgr::RenderScene(float etime)
{
	if (gameend == false && gamewin == false)
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glClearColor(1.0f, 1.0f, 1.0f, 1.0f);

		//float x = std::sin(time)*200.f;
		//float y = std::cos(time)*200.f;
		//time += 0.01f;

		//m_Renderer->DrawTextureRect(0, 0, 0, 1000, 500, 1, 1, 1, 1, StartBackGround);

		m_Renderer->DrawTextureRect(0, 0, 0, 1000, 500, 1, 1, 1, 1, BackGround);

		for (int i = 0; i < MAX_OBJECTS; ++i)
		{
			if (m_Objects[i] != NULL)
			{
				float x, y, z;
				m_Objects[i]->GetPosition(&x, &y, &z);
				float sizeX, sizeY, sizeZ;
				m_Objects[i]->GetSize(&sizeX, &sizeY, &sizeZ);
				float r, g, b, a;
				m_Objects[i]->GetColor(&r, &g, &b, &a);
				int type;
				m_Objects[i]->GetKind(&type);
				float hp;
				m_Objects[i]->GetHP(&hp);

				float newX, newY, newZ, newW, newH, newR, newG, newB, newA;
				newX = x * 100.f;
				newY = y * 100.f;
				newZ = z * 100.f;
				newW = sizeX * 100.f;
				newH = sizeY * 100.f;
				newR = r;
				newG = g;
				newB = b;
				newA = a;

				int seqX = 0;
				int seqY = 0;
				seqX = g_Seq % 9;
				seqY = (int)g_Seq / 9;
				g_Seq++;
				if (g_Seq > 80)
					g_Seq = 0;

				/*m_Renderer->DrawTextureRectSeqXY(
				newX, newY, newZ,
				newW, newH,
				newR, newG, newB, newA,
				m_TestExplodeTexture,
				seqX, seqY,
				9, 9);*/
				if (type == KIND_HERO)
				{
					if (hp <= 0)
						gameend = true;
					m_Renderer->DrawTextureRectHeight(
						newX, newY, 0,
						newW, newH,
						r, g, b, a,
						m_TestIsaacTexture, newZ);

					m_Renderer->DrawSolidRectGauge(
						newX, newY + newH, 0,
						newW, 3,
						1, 1, 1, 1,
						newZ,
						hp / 3000.f);
				}
				else if (type == KIND_BULLET)
				{
					m_Renderer->DrawTextureRectHeight(
						newX, newY, 0,
						newW, newH,
						r, g, b, a,
						m_TestExplodeTexture, newZ);
				}
				else if (type == KIND_ENEMYLITTLEDEVIL)
				{
					m_Renderer->DrawTextureRectHeight(
						newX, newY, 0,
						newW, newH,
						r, g, b, a,
						m_TestLittleDevilTexture, newZ);
					m_Renderer->DrawSolidRectGauge(
						newX, newY + newH, 0,
						30, 3,
						1, 1, 1, 1,
						newZ,
						hp / 200.f);
					LittleDevilMove(i, etime);
				}
				else if (type == KIND_ENEMYTURRET)
				{
					m_Renderer->DrawTextureRectHeight(
						newX, newY, 0,
						newW, newH,
						r, g, b, a,
						m_TestTurretTexture, newZ);
					m_Renderer->DrawSolidRectGauge(
						newX, newY + newH, 0,
						40, 3,
						1, 1, 1, 1,
						newZ,
						hp / 1000.f);
					TurretbulletShot(i, etime);
				}
				else if (type == KIND_BULLETFly)
				{
					m_Renderer->DrawTextureRectHeight(
						newX, newY, 0,
						newW, newH,
						r, g, b, a,
						m_TestFlyTexture, newZ);
				}
				else if (type == KIND_BOSS)
				{
					if (hp <= 0)
						gamewin = true;
					m_Renderer->DrawTextureRectHeight(
						newX, newY, 0,
						newW, newH,
						r, g, b, a,
						m_TestBossTexture, newZ);
					m_Renderer->DrawSolidRectGauge(
						newX, newY - newH - 230.f, 0,
						500, 10,
						1, 1, 1, 1,
						newZ,
						hp / 3000.f);
					BossMove(i, etime);
				}
				else if (type == KIND_BOSSPUNCH)
				{
					m_Renderer->DrawTextureRectHeight(
						newX, newY, 0,
						newW, newH,
						r, g, b, a,
						m_TestBossPunchTexture, newZ);
					PunchMove(i, etime);
				}
			}
		}
	}
}

void ScnMgr::AddObject(float px, float py, float pz, float sx, float sy, float sz, float vx, float vy, float vz, int kind, int HP, int state)
{
	int index = FindEmptyObjectSlot();

	if (index < 0)
	{
		std::cout << "Can't create object. \n";
		return;
	}

	m_Objects[index] = new ObjectA();
	m_Objects[index]->SetPosition(px, py, pz);
	m_Objects[index]->SetVelocity(vx, vy, vz);
	m_Objects[index]->SetAcc(0.f, 0.0f, 0.f);
	m_Objects[index]->SetSize(sx, sy, sz);
	//m_Objects[index]->SetForce(0.f, 0.f);
	m_Objects[index]->SetMass(0.05f);
	m_Objects[index]->SetFrictionCoef(0.5f); // 0.1f
	m_Objects[index]->SetColor(1.f, 1.f, 1.f, 1.f);
	m_Objects[index]->SetKind(kind);
	m_Objects[index]->SetHP(HP);
	m_Objects[index]->SetState(state);
}

void ScnMgr::AddEnemybyTime(float time)
{
	if (LittledevilTime == 0)
		LittledevilTime = time;
	if (FlyTime == 0)
		FlyTime = time;
	if (BossTime == 0)
		BossTime = time;
	if (Boss == true && PunchTime == 0)
		PunchTime = time;

	if ((time - LittledevilTime) > 5.f)
	{
		AddLittleDevil();
		LittledevilTime = 0.f;
	}
	if ((time - FlyTime) > 3.f)
	{
		FlyTime = 0.f;
	}
	if (Boss == true)
	{
		if ((time - PunchTime) > 3.f)
		{
			AddPunch();
			PunchTime = 0.f;
		}
	}
	if (BossTime >= 0 && (time - BossTime) > 20.f)
	{
		AddBoss();
		BossTime = -5.f;
		Boss = true;
	}
}
void ScnMgr::AddLittleDevil()
{
	AddObject(rand() % 8 - 4, -3.f, 0.0f,
		0.4f, 0.4f, 0.4f,
		0.f, 0.f, 0.f,
		KIND_ENEMYLITTLEDEVIL, 200.f,
		STATE_GROUND);
	AddObject(-5.f, rand() % 6 - 3, 0.0f,
		0.4f, 0.4f, 0.4f,
		0.f, 0.f, 0.f,
		KIND_ENEMYLITTLEDEVIL, 200.f,
		STATE_GROUND);
	AddObject(5.f, rand() % 6 - 3, 0.0f,
		0.4f, 0.4f, 0.4f,
		0.f, 0.f, 0.f,
		KIND_ENEMYLITTLEDEVIL, 200.f,
		STATE_GROUND);
}
void ScnMgr::LittleDevilMove(int ObjectNum, float etime)
{
	if (m_Objects[ObjectNum] == NULL)
		return;
	float hero_px, hero_py, hero_pz, enemy_px, enemy_py, enemy_pz;
	float forceX = 0.f, forceY = 0.f, forceZ = 0.f;
	int type;

	m_Objects[HERO_ID]->GetPosition(&hero_px, &hero_py, &hero_pz);
	m_Objects[ObjectNum]->GetPosition(&enemy_px, &enemy_py, &enemy_pz);

	float amount = 0.2f;
	if (hero_px > enemy_px)
		forceX += amount;
	if (hero_px < enemy_px)
		forceX -= amount;
	if (hero_py > enemy_py)
		forceY += amount;
	if (hero_py < enemy_py)
		forceY -= amount;

	m_Objects[ObjectNum]->SetPosition(enemy_px, enemy_py, enemy_pz);
	ApplyForce(forceX, forceY, forceZ, etime, ObjectNum);
}
void ScnMgr::AddTurret()
{
	AddObject(-4.f, 0.7f, 0.0f,
		0.4f, 0.4f, 0.4f,
		0.f, 0.f, 0.f,
		KIND_ENEMYTURRET, 1000.f,
		STATE_GROUND);
	AddObject(4.f, 0.7f, 0.0f,
		0.4f, 0.4f, 0.4f,
		0.f, 0.f, 0.f,
		KIND_ENEMYTURRET, 1000.f,
		STATE_GROUND);
	AddObject(-4.f, -1.3f, 0.0f,
		0.4f, 0.4f, 0.4f,
		0.f, 0.f, 0.f,
		KIND_ENEMYTURRET, 1000.f,
		STATE_GROUND);
	AddObject(4.f, -1.3f, 0.0f,
		0.4f, 0.4f, 0.4f,
		0.f, 0.f, 0.f,
		KIND_ENEMYTURRET, 1000.f,
		STATE_GROUND);
}
void ScnMgr::TurretbulletShot(int ObjectNum, float etime)
{
	if (FlyTime == 0.f)
	{
		float amount = 6.5f;
		float pX, pY, pZ;
		float sX, sY, sZ;
		float vX, vY, vZ;

		m_Objects[ObjectNum]->GetPosition(&pX, &pY, &pZ);
		m_Objects[ObjectNum]->GetVelocity(&vX, &vY, &vZ);
		sX = 0.2f;
		sY = 0.2f;
		sZ = 0.2f;


		if (pX > 0)
		{
			vX += -amount;
		}
		else
		{
			vX += amount;
		}

		AddObject(
			pX, pY, pZ,
			sX, sY, sZ,
			vX, vY, vZ,
			KIND_BULLETFly, 50.f, STATE_GROUND);
	}
}

void ScnMgr::AddBoss()
{
	m_Sound->PlaySound(m_SoundWarn, false, 3.f);

	AddObject(0, 2.8f, 0.0f,
		1.5f, 1.5f, 1.5f,
		0.f, 0.f, 0.f,
		KIND_BOSS, 3000.f,
		STATE_GROUND);
}

void ScnMgr::BossMove(int ObjectNum, float etime)
{
	if (m_Objects[ObjectNum] == NULL)
		return;
	float boss_px, boss_py, boss_pz;
	m_Objects[ObjectNum]->GetPosition(&boss_px, &boss_py, &boss_pz);


	float amount = 0.02f;
	if (boss_py > 1.5f)
		boss_py -= amount;

	m_Objects[ObjectNum]->SetPosition(boss_px, boss_py, boss_pz);
}

void ScnMgr::AddPunch()
{
	float randnum;
	randnum = rand() % 6 - 3;
	AddObject(randnum * -1, 2.f, 0.0f,
		1.f, 1.f, 1.f,
		0.f, 0.f, 0.f,
		KIND_BOSSPUNCH, 200.f,
		STATE_GROUND);
	AddObject(randnum, 2.f, 0.0f,
		1.f, 1.f, 1.f,
		0.f, 0.f, 0.f,
		KIND_BOSSPUNCH, 200.f,
		STATE_GROUND);
	m_Sound->PlaySound(m_SoundBossRoar, false, 3.f);
}

void ScnMgr::PunchMove(int ObjectNum, float etime)
{
	if (m_Objects[ObjectNum] == NULL)
		return;
	float punch_px, punch_py, punch_pz;
	m_Objects[ObjectNum]->GetPosition(&punch_px, &punch_py, &punch_pz);


	float amount = 0.05f;
	punch_py -= amount;

	m_Objects[ObjectNum]->SetPosition(punch_px, punch_py, punch_pz);
}


void ScnMgr::DeleteObject(unsigned int id)
{
	if (m_Objects[id])
	{
		delete m_Objects[id];
		m_Objects[id] = NULL; // 중요
	}
}

int ScnMgr::FindEmptyObjectSlot()
{
	for (int i = 0; i < MAX_OBJECTS; ++i)
	{
		if (m_Objects[i] == NULL)
			return i;
	}

	std::cout << "Object list is full. \n";
	return -1;
}
void ScnMgr::DoGarbageCollet()
{
	for (int i = 0; i < MAX_OBJECTS; i++)
	{
		if (m_Objects[i] == NULL)
			continue;
		
		int type;
		float vel, vx, vy, vz, px, py, pz, hp;
		m_Objects[i]->GetHP(&hp);
		m_Objects[i]->GetKind(&type);
		m_Objects[i]->GetVelocity(&vx, &vy, &vz);
		m_Objects[i]->GetPosition(&px, &py, &pz);


		vel = sqrtf(vx*vx + vy*vy + vz*vz);

		// check velocity
		if (vel < FLT_EPSILON && (type == KIND_BULLET || type == KIND_BULLETFly))
		{
			DeleteObject(i);
			continue;
		}

		// check hp
		if (hp <= 0 && (type == KIND_BULLET || type == KIND_ENEMYLITTLEDEVIL || type == KIND_ENEMYTURRET || type == KIND_BULLETFly))
		{
			DeleteObject(i);
			continue;
		}

		// check outofbound
		if (px < -5.f || px > 5.f || py < -2.5f || py > 2.5f)
		{
			if (type == KIND_BULLET || type == KIND_BOSSPUNCH)
			{
				DeleteObject(i);
				continue;
			}
		}
	}
}
void ScnMgr::Shoot(int shootID)
{
	if (shootID == SHOOT_NONE)
	{
		return;
	}

	float amount = 10.0f;
	float pX, pY, pZ;
	float sX, sY, sZ;
	float vX, vY, vZ;

	m_Objects[HERO_ID]->GetPosition(&pX, &pY, &pZ);
	m_Objects[HERO_ID]->GetVelocity(&vX, &vY, &vZ);
	//m_Objects[HERO_ID]->GetSize(&sX, &sY);
	sX = 0.1f;
	sY = 0.1f;
	sZ = 0.1f;

	switch (shootID)
	{
	case SHOOT_LEFT:
		vX += -amount;
		vY += 0.f;
		break;
	case SHOOT_RIGHT:
		vX += amount;
		vY += 0.f;
		break;
	case SHOOT_UP:
		vX += 0.f;
		vY += amount;
		break;
	case SHOOT_DOWN:
		vX += 0.f;
		vY += -amount;
		break;
	}

	AddObject(
		pX, pY, pZ,
		sX, sY, sZ,
		vX, vY, vZ, 
		KIND_BULLET, 50, STATE_GROUND);

	m_Sound->PlaySound(m_SoundFire, false, 1.f);
}


void ScnMgr::Update(float eTimeInSec)
{
	for (int i = 0; i < MAX_OBJECTS; ++i)
	{
		if (m_Objects[i])
			m_Objects[i]->Update(eTimeInSec);
	}
}

void ScnMgr::ApplyForce(float x, float y, float z, float time, int Objectnum)
{
	int state;
	m_Objects[Objectnum]->GetState(&state);

	if (state == STATE_AIR)
	{
		z = 0;
	}

	m_Objects[Objectnum]->ApplyForce(x, y, z, time);
}


bool ScnMgr::BBCollision(float minx, float miny,
	float maxx, float maxy,
	float minz, float maxz,
	float minx1, float miny1,
	float maxx1, float maxy1,
	float minz1, float maxz1)
{
	if (minx > maxx1)
	{
		return false;
	}

	if (maxx < minx1)
	{
		return false;
	}

	if (miny > maxy1)
	{
		return false;
	}

	if (maxy < miny1)
	{
		return false;
	}

	if (minz > maxz1)
	{
		return false;
	}

	if (maxz < minz1)
	{
		return false;
	}

	return true;

	//if (minx > minx1)
	//	return false;
	//if (maxx < maxx1)
	//	return false;
	//if (miny > miny1)
	//	return false;
	//if (maxy < maxy1)
	//	return false;

	//if (minx < minx1)
	//	return true;
	//if (maxx > maxx1)
	//	return true;
	//if (miny < miny1)
	//	return true;
	//if (maxy > maxy1)
	//	return true;
}

void ScnMgr::UpdateCollision()
{
	int collisionCount = 0;

	for (int i = 0; i < MAX_OBJECTS; ++i)
	{
		collisionCount = 0;

		for (int j = i + 1; j < MAX_OBJECTS; ++j)
		{
			if (m_Objects[i] != NULL && m_Objects[j] != NULL && i != j)
			{
				float minx, maxx, miny, maxy, minz, maxz = 0.f; // i index
				float minx1, maxx1, miny1, maxy1, minz1, maxz1 = 0.f; // j index

				float px, py, pz, sx, sy, sz = 0.f;

				m_Objects[i]->GetPosition(&px, &py, &pz);
				m_Objects[i]->GetSize(&sx, &sy, &sz);

				minx = px - sx / 2.f; maxx = px + sx / 2.f;
				miny = py - sy / 2.f; maxy = py + sy / 2.f;
				minz = pz - sz / 2.f; maxz = pz + sz / 2.f;

				//float px, py, pz, sx, sy = 0.f;

				m_Objects[j]->GetPosition(&px, &py, &pz);
				m_Objects[j]->GetSize(&sx, &sy, &sz);

				minx1 = px - sx / 2.f; maxx1 = px + sx / 2.f;
				miny1 = py - sy / 2.f; maxy1 = py + sy / 2.f;
				minz1 = pz - sz / 2.f; maxz1 = pz + sz / 2.f;

				if (BBCollision(minx, miny, maxx, maxy, minz, maxz, minx1, miny1, maxx1, maxy1, minz1, maxz1))
				{
					collisionCount++;
					//process collision
					ProcessCollision(i, j);
				}
			}
		}

		if (collisionCount > 0)
		{
			m_Objects[i]->SetColor(1.f, 0.f, 0.f, 1.f);
		}
		else if(m_Objects[i] != NULL)
		{
			m_Objects[i]->SetColor(1, 1, 1, 1);
		}
	}
}

void ScnMgr::ProcessCollision(int i, int j)
{
	ObjectA *obj1 = m_Objects[i];
	ObjectA *obj2 = m_Objects[j];

	if (obj1 == NULL || obj2 == NULL)
	{
		std::cout << "obj " << i << ", obj " << j << ", one of these is null" << std::endl;
	}

	int kind1, kind2;
	obj1->GetKind(&kind1);
	obj2->GetKind(&kind2);

	if (kind1 == KIND_HERO && kind2 == KIND_ENEMYLITTLEDEVIL)
	{
		float hp1, hp2;
		obj1->GetHP(&hp1); // hero
		obj2->GetHP(&hp2); // littledevil

		float newHP = hp1 - 1.f;

		obj1->SetHP(newHP);
		m_Sound->PlaySound(m_SoundHit, false, 3.f);
	}
	if (kind1 == KIND_HERO && (kind2 == KIND_BULLETFly || kind2 == KIND_BOSSPUNCH))
	{
		float hp1, hp2;
		obj1->GetHP(&hp1); // hero
		obj2->GetHP(&hp2); // bulletfly

		float newHP = hp1 - hp2;
		std::cout << hp1 << "\n";

		obj1->SetHP(newHP);
		obj2->SetHP(0);
		m_Sound->PlaySound(m_SoundHit, false, 3.f);
	}
	if ((kind1 == KIND_ENEMYLITTLEDEVIL && kind2 == KIND_BULLET) || (kind1 == KIND_ENEMYTURRET && kind2 == KIND_BULLET) || (kind1 == KIND_BOSS && kind2 == KIND_BULLET))
	{
		float hp1, hp2;
		obj1->GetHP(&hp1); // building
		obj2->GetHP(&hp2); // bullet

		float newHP = hp1 - hp2;


		obj1->SetHP(newHP);
		obj2->SetHP(0);
		m_Sound->PlaySound(m_SoundExplosion, false, 3.f);
	}

	if ((kind2 == KIND_ENEMYLITTLEDEVIL && kind1 == KIND_BULLET) || (kind2 == KIND_ENEMYTURRET && kind1 == KIND_BULLET) || (kind2 == KIND_BOSS && kind1 == KIND_BULLET))
	{
		float hp1, hp2;
		obj1->GetHP(&hp1); // bullet
		obj2->GetHP(&hp2); // building

		float newHP = hp2 - hp1;

		obj1->SetHP(0);
		obj2->SetHP(newHP);
		m_Sound->PlaySound(m_SoundExplosion, false, 3.f);
	}
}