#pragma once // 한번만 불리도록

#define HERO_ID 0
#define MAX_OBJECTS 300

#define KIND_HERO 0
#define KIND_BULLET 1
#define KIND_ENEMYLITTLEDEVIL 2
#define KIND_ENEMYTURRET 3
#define KIND_BULLETFly 4
#define KIND_BOSS 5

#define KIND_BOSSPUNCH 6

#define SHOOT_NONE -1
#define SHOOT_LEFT 1
#define SHOOT_RIGHT 2
#define SHOOT_UP 3
#define SHOOT_DOWN 4

#define STATE_GROUND 0
#define STATE_AIR 1

#define GRAVITY 9.8