#include "stdafx.h"
#include "GameOver.h"
#include "Renderer.h"
#include "Dependencies\glew.h"


LOSE::LOSE(int state)
{
	m_Renderer = new Renderer(1000, 500);

	gamestate = state;
	LoseBackGround = m_Renderer->CreatePngTexture("./gameover.png");
	WinBackGround = m_Renderer->CreatePngTexture("./gamewin.png");
}


LOSE::~LOSE()
{
}

void LOSE::RenderScene(float etime)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);

	if(gamestate == 0)
		m_Renderer->DrawTextureRect(0, 0, 0, 1000, 500, 1, 1, 1, 1, LoseBackGround);
	else
		m_Renderer->DrawTextureRect(0, 0, 0, 1000, 500, 1, 1, 1, 1, WinBackGround);
}