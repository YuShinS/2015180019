#include "stdafx.h"
#include "ObjectA.h"
#include "math.h"
#include "float.h"
#include "global.h"

ObjectA::ObjectA()
{
}

ObjectA::~ObjectA()
{
}

void ObjectA::GetPosition(float * x, float * y, float *z)
{
	*x = pos_X;
	*y = pos_Y;
	*z = pos_Z;
}

void ObjectA::SetPosition(float x, float y, float z)
{
	pos_X = x;
	pos_Y = y;
	pos_Z = z;
}

void ObjectA::GetSize(float *sx, float *sy, float *sz)
{
	*sx = sizeX;
	*sy = sizeY;
	*sz = sizeZ;
}

void ObjectA::SetSize(float sx, float sy, float sz)
{
	sizeX = sx;
	sizeY = sy;
	sizeZ = sz;
}

void ObjectA::GetVelocity(float * x, float * y, float *z)
{
	*x = m_VelX;
	*y = m_VelY;
	*z = m_VelZ;
}

void ObjectA::SetVelocity(float x, float y, float z)
{
	m_VelX = x;
	m_VelY = y;
	m_VelZ = z;
}

void ObjectA::GetAcc(float * vx, float * vy, float *vz)
{
	*vx = m_AccX;
	*vy = m_AccY;
	*vz = m_AccZ;
}

void ObjectA::SetAcc(float vx, float vy, float vz)
{
	m_AccX = vx;
	m_AccY = vy;
	m_AccZ = vz;
}

//void ObjectA::GetForce(float * x, float * y)
//{
//	*x = m_ForceX;
//	*y = m_ForceY;
//}
//
//void ObjectA::SetForce(float x, float y)
//{
//	m_ForceX = x;
//	m_ForceY = y;
//}

void ObjectA::GetMass(float * x)
{
	*x = m_Mass;
}

void ObjectA::SetMass(float x)
{
	m_Mass = x;
}

void ObjectA::GetFrictionCoef(float * x)
{
	*x = m_coefFriction;
}

void ObjectA::SetFrictionCoef(float x)
{
	m_coefFriction = x;
}

// 1pixel = 1cm ; 최종적으로 픽셀 변환을 해줘야함

void ObjectA::Update(float eTimeInSec)
{
	// Calc friction
	float gz = m_Mass * 9.8f;
	float friction = m_coefFriction * gz; // 마찰력 = 마찰계수 X 힘의 크기

	float velMag = sqrtf(m_VelX * m_VelX + m_VelY * m_VelY + m_VelZ * m_VelZ);

	if (velMag < FLT_EPSILON) // 마찰력이 없음
	{
		m_VelX = 0.f;
		m_VelY = 0.f;
		m_VelZ = 0.f;
	}
	else
	{
		// 방향
		// 방향에 비례해서 마찰력이 곱해질 것으므로 방향만 필요 => 정규화(벡터의 크기 1로) 필요
		float fx = -friction*m_VelX / velMag;
		float fy = -friction*m_VelY / velMag;

		// 가속도
		float accx = fx / m_Mass;
		float accy = fy / m_Mass;

		// 속도
		float newVelX = m_VelX + accx * eTimeInSec;
		float newVelY = m_VelY + accy * eTimeInSec;

		if (newVelX*m_VelX < 0.f)
		{
			m_VelX = 0.f;
		}
		else
		{
			m_VelX = newVelX;
		}
		if (newVelY*m_VelY < 0.f)
		{
			m_VelY = 0.f;
		}
		else
		{
			m_VelY = newVelY;
		}

		// gravity calculation
		m_VelZ = m_VelZ - GRAVITY * eTimeInSec;
		
	}

	// Calc velocity
	m_VelX = m_VelX + m_AccX * eTimeInSec;
	m_VelY = m_VelY + m_AccY * eTimeInSec;
	m_VelZ = m_VelZ + m_AccZ * eTimeInSec;

	// Calc location
	pos_X = pos_X + m_VelX * eTimeInSec;
	pos_Y = pos_Y + m_VelY * eTimeInSec;
	pos_Z = pos_Z + m_VelZ * eTimeInSec;

	if (pos_Z > 0.f)
	{
		m_State = STATE_AIR;
	}
	else
	{
		m_State = STATE_GROUND;
		pos_Z = 0.f;
		m_VelZ = 0.f;
	}

	// add api later
	//dstX = dstX * 2.f;
	//dstY = dstY * 2.f;

	SetPosition(pos_X, pos_Y, pos_Z);
}

void ObjectA::ApplyForce(float x, float y, float z, float time)
{
	// Calc acc
	m_AccX = x / m_Mass;
	m_AccY = y / m_Mass;
	m_AccZ = z / m_Mass;

	// Calc vel
	m_VelX = m_VelX + m_AccX * time;
	m_VelY = m_VelY + m_AccY * time;
	m_VelZ = m_VelZ + m_AccZ * time;

	m_AccX = 0.f;
	m_AccY = 0.f;
	m_AccZ = 0.f;
}

void ObjectA::GetColor(float *r, float *g, float *b, float *a)
{
	*r = m_R;
	*g = m_G;
	*b = m_B;
	*a = m_A;
}

void ObjectA::SetColor(float r, float g, float b, float a)
{
	m_R = r;
	m_G = g;
	m_B = b;
	m_A = a;
}

void ObjectA::GetKind(int * kind)
{
	*kind = m_Kind;
}

void ObjectA::SetKind(int kind)
{
	m_Kind = kind;
}

void ObjectA::GetHP(float * hp)
{
	*hp = m_HP;
}

void ObjectA::SetHP(float hp)
{
	m_HP = hp;
}
void ObjectA::GetState(int *state)
{
	*state = m_State;
}

void ObjectA::SetState(int state)
{
	m_State = state;
}